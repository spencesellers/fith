package s.spencer.fith;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import s.spencer.fith.values.Value;

public class HeapScope {
	private final Map<String, Value> table;
	private HeapScope parentScope;
	public HeapScope(){
		table = new HashMap<String, Value>();
	}
	public HeapScope(HeapScope parent){
		this.parentScope = parent;
		table = new HashMap<String, Value>();
	}
	public void assignValue(String var, Value v){
		assert(v != null): "Assigning null value to a HeapScope";
		assert(!v.equals("")): "Var name is empty string!";
		//System.out.println(this.toString() + " Assigning value " + v + " as " + var);
		if (this.hasVar(var)){ //It exists in this scope.
			table.put(var, v);
		} else if ( this.canFind(var)){ //It exists, but not in this exact scope. 
			parentScope.assignValue(var, v);
		} else{ //Variable doesn't exist yet. 
			table.put(var, v);
		}
		
	}
	public String[] getAllIdentifiers(){
		 Iterator<String> iter = table.keySet().iterator();
		 String[] strs = new String[table.size()];
		 for (int i = 0; i < strs.length; i++){
			 strs[i] = iter.next();
		 }
		 return strs;
	}
	public void localDelete(String var){
		table.remove(var);
	}
	public Value getValue(String var){
		if (table.containsKey(var)){
			return table.get(var);
		}
		if (parentScope != null){
			return parentScope.getValue(var);
		}
		return null;
	}
	public boolean hasVar(String var){
		return table.containsKey(var);
	}
	public boolean canFind(String var){
		if (table.containsKey(var)){
			return true;
		}
		if (parentScope != null){
			return parentScope.canFind(var);
		} else {
			return false;
		}
	}
}
