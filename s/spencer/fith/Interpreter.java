package s.spencer.fith;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import s.spencer.fith.values.Value;

public class Interpreter {
	LinkedList<Value> stack;
	HeapScope scope;
	public Interpreter(){
		stack = new LinkedList<Value>();
		scope = new HeapScope();
		this.execStr(readStdLib());
	}
	private String readStdLib(){
		InputStream in = getClass().getResourceAsStream("/resources/StandardLib.5");
		java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	public static void main(String[] args){
		
		Interpreter i = new Interpreter();
		if (args.length > 0){
			//We're running a file
			i.execFile(args[0]);
		} else{
			//We're interpreting live
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
			while(true){
				try {
					i.execStr(br.readLine());
				} catch (IOException e) {
					System.out.println("User Input read error!");
					e.printStackTrace();
				}
			}
		}
	}
	public void execFile(String filename){
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
			e.printStackTrace();
			System.exit(0);
		}
		String line = null;
		while (true){
			try {
				line = reader.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(0);
			}
			if (line == null){
				break;
			}
			execStr(line);
		}
	}
	public void execStr(String str){
		Parser.parse(str).exec(stack, scope);
	}
	
	
}
