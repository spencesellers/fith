package s.spencer.fith;

import java.util.Iterator;

public class LinkedList<Item> implements Iterable<Item>{
	private class Node {
		private Node next = null;
		private Node prev = null;
		public Item value;
		public Node(Item x){
			value = x;
		}
		public Node next(){
			return next;
		}
		public Node prev(){
			return prev;
		}
		public void setNext(Node n){
			next = n;
		}
		public void setPrev(Node n){
			prev = n;
		}
		public void insertAfter(Node n){
			n.setPrev(this);
			n.setNext(next);
			this.next = n;
			if (this.prev != null){
				assert(this.prev.next.next == n): "Something doesn't line up";
				assert(this.prev.next == this);
			}
			
		}
		public void insertBefore(Node n){
			if (prev != null){
				prev.setNext(n);
			}
			n.setPrev(prev);
			this.setPrev(n);
			n.setNext(this);
		}
		private void delete(){ 
			//Dangerous, because if this is the first element in the list
			//The whole list will be destroyed;
			if (prev != null){
				prev.setNext(next);
			}
			if (next != null){
				next.setPrev(prev);
			}
		}
	}
	public class LinkedListIterator implements Iterator<Item>{
		Node current;
		LinkedList<Item> ll;
		public LinkedListIterator(LinkedList<Item> l){
			this.ll = l;
			
		}
		public boolean hasNext(){
			if (current == null){
				if (ll.isEmpty()){
					return false;
				}
				return true;
			}
			if (current.next() != null){
				return true;
			} else {
				return false;
			}
		}
		public Item next(){
			if (current == null){
				current = ll.first;
				return current.value;
			}
			current = current.next();
			return current.value;
			
		}
		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
		
		
	}
	Node first;
	Node last;
	public LinkedList(){
		
	}
	public LinkedList(LinkedList<Item> ll){
		while(!ll.isEmpty()){
			this.push(ll.popFront());
		}
	}
	public LinkedList(Item[] array){
		for(int i = 0; i < array.length; i++){
			this.push(array[i]);
		}
	}
	@SuppressWarnings("unused")
	private Node getLastNode(){
		return last;
//		if (first == null){
//			System.out.println("Empty List!!!");
//			return null;
//		}
//		for (Node i = first; i != null; i = i.next()){
//			//System.out.println("Is i last?");
//			if (i.next() == null){
//				//System.out.println("Found last!!");
//				return i;
//			}
//		}
//		System.out.println("Uh oh");
//		return null;
	}
	@SuppressWarnings("unused")
	private void remove(Node n){
		if (n == this.first){
			this.first = n.next();
		}
		if (n == last){
			this.last = n.prev();
		}
		n.delete();
	}
	public int length(){
		int counter = 0;
		for (Item i : this){
			counter ++;
		}
		return counter;
	}
	public void set(Item item, int index){
		Node n = first;
		for (int i = 0; i < index; i++){
			n = n.next();
			
		}
		n.value = item;
	}
	public Item get(int index){
		Node n = first;
		for (int i = 0; i < index; i++){
			n = n.next();
			if (n == null){
				return null;
			}
		}
		return n.value;
	}
	public void remove(int index){
		Node n = first;
		for (int i = 0; i < index; i++){
			n = n.next();
			if (n == null){
				return;
			}
		}
		this.remove(n);
	}
	public void push(Item x){
		assert (x != null): "Pushing a null value!";
		Node n = new Node(x);
		if (first == null){
			first = n;
			last = n;
		} else {
			assert (last != null): "Last element is null, despite first being not!";
			//System.out.println("First is not null. ");
			last.insertAfter(n);
			assert (last.next().value == x): "The last... isn't the last.";
			assert (last != null);
			last = n;
		}
		assert(last != null);
		assert(first != null);
	}
	public void pushFront(Item x){
		assert (x != null): "Pushing a null value!";
		Node n = new Node(x);
		if (first == null){
			first = n;
			last = n;
		} else {
			assert (last != null): "Last element is null, despite first being not!";
			first.insertBefore(n);
			assert (last != null);
			first = n;
		}
		assert(last != null);
		assert(first != null);
	}
	public Item pop(){
		if (this.isEmpty()){
			return null;
		}
		Node n = last;
		last = n.prev();
		if (n == first){
			first = null;
		} else{
			last.setNext(null);
		}
		return n.value;
		
	}
	public Item popFront(){
		Node n = first;
		first = n.next();
		n.delete();
		return n.value;
	}
	public Item peek(){
		if (last == null){
			return null;
		}
		return last.value;
	}
	public Item peekFront(){
		return first.value;
	}
	public boolean isEmpty(){
		if(first != null){
			assert(last != null);
		}
		return first == null;
	}
	public int count(Item x){
		int c = 0;
		for (Node i = first; i != null; i.next()){
			if(i.value.equals(x)){
				c++;
			}
		}
		return c;
	}
	public boolean contains(Item x){
		for (Node i = first; i != null; i = i.next()){
			if(i.value.equals(x)){
				return true;
			}
		}
		return false;
	}
	public String toString(){
		String str = "[ ";
		for (Node i = first; i != null; i = i.next()){
			str = str + ", " + i.value.toString();
		}
		return str + "]";
	}
	public static void main(String[] args){
		LinkedList<String> l = new LinkedList<String>();
		l.push("This");
		l.push("Is");
		l.push("A");
		l.push("Test");
		l.pushFront("FIRST");
		l.set("AAA", 2);
		System.out.println(l.get(0));
		System.out.println(l.get(2));
		System.out.println(l.pop());
		System.out.println(l.pop());
		System.out.println(l.pop());
		System.out.println(l.pop());
		
	}
	@Override
	public Iterator<Item> iterator() {
		return new LinkedListIterator(this);
	}
}
