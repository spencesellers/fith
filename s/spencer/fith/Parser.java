package s.spencer.fith;
import s.spencer.fith.tokens.*;
import s.spencer.fith.values.*;

import java.util.Iterator;
import java.util.regex.*;
public class Parser {
	public static Token parse(String str){
		return parse(split(str));
	}
	public static LinkedList<String> split(String str){
		LinkedList<String> ll = new LinkedList<String>();
		// This regex splits by whitespace, but preserves quotes and tilde comments.
		Pattern p = Pattern.compile("\".*?\"|~.*?~|[^\\s]*|\\{|\\}");
		Matcher matcher = p.matcher(str);
		while (matcher.find()){
			String cmd = matcher.group();
			if (cmd.length() > 0){
				ll.push(cmd);
			}			
		}
		return ll;
	}
	public static Token parse(LinkedList<String> strs){
		//LinkedList<ExprToken> exprStack = new LinkedList<ExprToken>();
		Iterator<String> i = strs.iterator();
		LinkedList<Expr> scopeStack = new LinkedList<Expr>();
		scopeStack.push(new Expr()); //Sets up a new expression. 
		while (i.hasNext()){
			String cmd = i.next();
			if (cmd.matches("-?[0-9]+(\\.[0-9]*)?")){ //A Number
				scopeStack.peek().push(new ConstantToken(Double.parseDouble(cmd)));
			} else if (cmd.matches("\".*\"")){ //A String Literal
					scopeStack.peek().push(new ConstantToken(new StringValue(cmd.substring(1, cmd.length() - 1))));
			} else if (cmd.matches("~.*")){ //A comment.
				//Do nothing. 
			}
			else if (cmd.matches("\\$.+")){ //An Identifier
				scopeStack.peek().push(new IdentifierToken(cmd.substring(1)));
			} else if (cmd.matches("![a-zA-Z0-9]+")){ //Identifier Function call. Syntactic Sugar.
				scopeStack.peek().push(new FunctionCallToken(cmd.substring(1)));
			} else if (cmd.matches("@!.+")){ 
				//Syntactic Sugar for running a ScopeValue's method.
				scopeStack.peek().push(new IdentifierToken(cmd.substring(2)));
				scopeStack.peek().push(new ExecScopedToken());
			} else if (cmd.matches("#.+")){ 
				//Syntactic Sugar for Unpacking.
				scopeStack.peek().push(new IdentifierToken(cmd.substring(1)));
				scopeStack.peek().push(new UnpackToken());
			} else if (cmd.equals("true")){
				scopeStack.peek().push(new ConstantToken(new BooleanValue(true)));
			} else if (cmd.equals("false")){
				scopeStack.peek().push(new ConstantToken(new BooleanValue(false)));
			} else if (cmd.equals("=") || cmd.equals("def")){
				scopeStack.peek().push(new VariableSetToken());
			} else if (cmd.equals("|")){
				//Creates an unscoped expression. Fairly useless.
				scopeStack.push(new Expr());
			} else if (cmd.equals("{")){ //Begins expression.
				scopeStack.push(new ScopedExpr());
			} else if (cmd.equals("}")){ //'Compiles' latest scope into a single expression.
				Expr e = scopeStack.pop();
				scopeStack.peek().push(new ConstantToken(new ExecValue(e)));
			} else if (cmd.equals("+")){
				scopeStack.peek().push(new AdditionToken());
			} else if (cmd.equals("-")){
				scopeStack.peek().push(new SubtractionToken());
			} else if (cmd.equals("*")){
				scopeStack.peek().push(new MultiplyToken());
			}else if (cmd.equals("sqrt")){
				scopeStack.peek().push(new SqrtToken());
			} else if (cmd.equals(">")){
				scopeStack.peek().push(new GreaterThanToken());
			} else if (cmd.equals("<")){
				scopeStack.peek().push(new LessThanToken());
			} else if (cmd.equals("==")){
				scopeStack.peek().push(new EqualToToken());
			} else if (cmd.equals("!")){
				scopeStack.peek().push(new ExecFunctionToken());
			} else if (cmd.equals("#")){ //Unpack
				scopeStack.peek().push(new UnpackToken());
			} else if (cmd.equals("@@")){ //New Scope
				scopeStack.peek().push(new NewScopeValueToken());
			} else if (cmd.equals("@!")){
				scopeStack.peek().push(new ExecScopedToken());
			} else if (cmd.equals("@=")){
				scopeStack.peek().push(new SetScopedToken());
			} else if (cmd.equals("!@")){
				scopeStack.peek().push(new ExecInScopeToken());
			} else if (cmd.equals("print")){
				scopeStack.peek().push(new PrintToken());
			} else if (cmd.equals("mod")){
				scopeStack.peek().push(new ModToken());
			} else if (cmd.equals("pow")){
				scopeStack.peek().push(new PowToken());
			} else if (cmd.equals("dup")){
				scopeStack.peek().push(new DupToken());
			} else if (cmd.equals("free")){
				scopeStack.peek().push(new FreeToken());
			} else if (cmd.equals("ndup")){
				scopeStack.peek().push(new NDupToken());
			} else if (cmd.equals("del")){
				scopeStack.peek().push(new DelToken());
			} else if (cmd.equals("map")){
				scopeStack.peek().push(new MapToken());
			} else if (cmd.equals("zipwith")){
				scopeStack.peek().push(new ZipWithToken());
			} else if (cmd.equals("while")){
				scopeStack.peek().push(new WhileToken());
			} else if (cmd.equals("for")){
				scopeStack.peek().push(new ForToken());
			} else if (cmd.equals("foreach")){
				scopeStack.peek().push(new ForEachToken());
			} else if (cmd.equals("ifthenelse")){
				scopeStack.peek().push(new IfElseToken());
			}  else if (cmd.equals("ifthen")){
				scopeStack.peek().push(new IfToken());
			} else if (cmd.equals("tostring")){
				scopeStack.peek().push(new ToStringToken());
			} else if (cmd.equals("input")){
				scopeStack.peek().push(new InputToken());
			} else if (cmd.equals("swap")){
				scopeStack.peek().push(new SwapToken());
			} else if (cmd.equals("parsefloat")){
				scopeStack.peek().push(new ParseFloatToken());
			} else if (cmd.equals("get")){
				scopeStack.peek().push(new GetToken());
			} else if (cmd.equals("set")){
				scopeStack.peek().push(new SetIndexToken());
			} else if (cmd.equals("remove")){
				scopeStack.peek().push(new RemoveFromListToken());
			} else if (cmd.equals("newlist") || cmd.equals("++")){
				scopeStack.peek().push(new MakeListToken());
			} else if (cmd.equals(".dump")){
				scopeStack.peek().push(new DebugDumpToken());
			} else {
				System.out.println("Warning: keyword \"" + cmd + "\" not understood.");
			}
		}
		return scopeStack.pop();
	}
}
