package s.spencer.fith.tokens;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.*;

public class ConstantToken implements Token{
	final Value value;
	public ConstantToken(Value v){
		this.value = v;
	}
	public ConstantToken(Double d){
		this.value = new FloatValue(d);
	}
	public void exec(LinkedList<Value> stack, HeapScope scope){
		stack.push(value);
	}
	public String toString(){ return "(constant " + value + ")";}
}
