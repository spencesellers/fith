package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.ExecValue;
import s.spencer.fith.values.IdentifierValue;
import s.spencer.fith.values.Value;

public class ExecFunctionToken implements Token{

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		Value v = stack.pop();
		if (v == null){
			System.out.println("Error: No identifier specified, got null instead.");
		}
		if (v instanceof IdentifierValue){
			IdentifierValue idv = (IdentifierValue) v;
			Value value = scope.getValue(idv.getIdentifier());
			if (value == null){
				System.out.println("Error: Variable " + idv.getIdentifier() + " is not present.");
				return;
			}
			if (value instanceof ExecValue){
				((ExecValue) value).exec(stack, scope);
			} else {
				stack.push(value);
			}
			//this.exec(stack, scope);
		} else if (v instanceof ExecValue){
			ExecValue fv = (ExecValue) v;
			//System.out.println("Running " + fv);
			fv.exec(stack, scope);
		} else {
			System.out.println("Invalid value for execution: " + v.toString());
		}
		
	}
	
}
