package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.ExecValue;
import s.spencer.fith.values.ScopeValue;
import s.spencer.fith.values.Value;

public class ExecInScopeToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		ExecValue ev = (ExecValue) stack.pop();
		ScopeValue sv = (ScopeValue) stack.pop();
		ev.execInScope(stack, sv.scope);
		stack.push(sv);		
	}

}
