package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.IdentifierValue;
import s.spencer.fith.values.ScopeValue;
import s.spencer.fith.values.Value;

public class ExecScopedToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		IdentifierValue id = (IdentifierValue) stack.pop();
		ScopeValue sv = (ScopeValue) stack.pop();
		sv.execIdentifier(id.getIdentifier(), stack);
		
	}

}
