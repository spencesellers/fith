package s.spencer.fith.tokens;

import java.util.Iterator;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.Value;

public class Expr implements Token{
	
	private LinkedList<Token> items;
	public Expr(){
		items = new LinkedList<Token>();
	}
	public Expr(LinkedList<Token> exprs){
		items = new LinkedList<Token>(exprs);
	}
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		Iterator<Token> i = items.iterator();
		while (i.hasNext()){
			i.next().exec(stack, scope);
		}
		
	}
	public void push(Token t){ //Used during compilation.
		items.push(t);
	}
	public Token pop(){
		return items.pop();
	}
	public Expr concat(Expr e){
		LinkedList<Token> tokens = new LinkedList<Token>(items);
		for(Token t : e.items){
			tokens.push(t);
		}
		return new Expr(tokens);
	}
	public String toString(){
		return "{ " + items.toString() + "}";
	}

}
