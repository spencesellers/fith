package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.ExecValue;
import s.spencer.fith.values.FloatValue;
import s.spencer.fith.values.Value;

public class ForToken implements Token {
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		ExecValue inside = (ExecValue) stack.pop();
		int n = ((FloatValue) stack.pop()).getDouble().intValue();
		for (int i = 0; i < n; i++){
			stack.push(new FloatValue((double) i));
			inside.exec(stack, scope);
		}

	}

}
