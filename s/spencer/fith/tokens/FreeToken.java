package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.IdentifierValue;
import s.spencer.fith.values.Value;

public class FreeToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		IdentifierValue idv = (IdentifierValue) stack.pop();
		scope.localDelete(idv.getIdentifier());
		

	}

}
