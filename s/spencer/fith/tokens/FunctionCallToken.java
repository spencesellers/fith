package s.spencer.fith.tokens;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.IdentifierValue;
import s.spencer.fith.values.Value;

public class FunctionCallToken implements Token {
	private final String identifier;
	public FunctionCallToken(String identifier){
		this.identifier = identifier;
	}
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		stack.push(new IdentifierValue(identifier));
		Token t = new ExecFunctionToken();
		t.exec(stack, scope);
		//scope.getToken(identifier).exec(stack, scope);

	}

}
