package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.*;

public class GetToken implements Token{

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		FloatValue f = (FloatValue) stack.pop();
		ListValue l = (ListValue) stack.peek();
		Value v = l.get(f);
		if (v == null){
			System.out.println("Warning: Fetching a null value from the list.");
		} else {
			stack.push(v);
		}
	}

}
