package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.BooleanValue;
import s.spencer.fith.values.Value;

public class GreaterThanToken implements Token {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		Value a =  stack.pop();
		Value b = stack.pop();
		Comparable cb = (Comparable) b;
		if (cb.compareTo(a) < 0){
			stack.push(new BooleanValue(true));
		} else {
			stack.push(new BooleanValue(false));
		}

	}

}
