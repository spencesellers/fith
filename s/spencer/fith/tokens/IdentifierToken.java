package s.spencer.fith.tokens;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.IdentifierValue;
import s.spencer.fith.values.Value;

public class IdentifierToken implements Token {
	public final String identifier;
	public IdentifierToken(String id){
		this.identifier = id;
	}
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		stack.push(new IdentifierValue(this.identifier));
	}

}
