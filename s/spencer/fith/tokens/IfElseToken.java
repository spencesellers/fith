package s.spencer.fith.tokens;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.ExecValue;
import s.spencer.fith.values.Value;

public class IfElseToken implements Token {
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		ExecValue elseexec = (ExecValue) stack.pop();
		ExecValue then = (ExecValue) stack.pop();
		ExecValue condition = (ExecValue) stack.pop();
		condition.exec(stack, scope);
		if(stack.pop().toBoolean()){
			then.exec(stack, scope);
		} else {
		elseexec.exec(stack, scope);
		}

	}

}
