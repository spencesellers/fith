package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.ExecValue;
import s.spencer.fith.values.Value;

public class IfToken extends IfElseToken{
	public IfToken(){
		super();
	}
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope){
		stack.push(new ExecValue(new Expr()));
		super.exec(stack, scope);
	}
}
