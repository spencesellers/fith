package s.spencer.fith.tokens;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.StringValue;
import s.spencer.fith.values.Value;

public class InputToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		System.out.print("--> ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input;
		try {
			input = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			input = null;
		}
		stack.push(new StringValue(input));

	}

}
