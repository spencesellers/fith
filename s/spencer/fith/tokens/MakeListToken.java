package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.ListValue;
import s.spencer.fith.values.Value;

public class MakeListToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		stack.push(new ListValue());

	}

}
