package s.spencer.fith.tokens;
import java.util.Iterator;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.ExecValue;
import s.spencer.fith.values.ListValue;
import s.spencer.fith.values.Value;

public class MapToken implements Token {
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		ExecValue func = (ExecValue) stack.pop();
		ListValue lv = (ListValue) stack.pop();
		Iterator<Value> i = lv.getLinkedList().iterator();
		ListValue result = new ListValue();
		while (i.hasNext()){
			stack.push(i.next());
			func.exec(stack, scope);
			result.push(stack.pop());
		}
		stack.push(result);
	}

}
