package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.FloatValue;
import s.spencer.fith.values.Value;

public class ModToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		FloatValue a = (FloatValue) stack.pop();
		FloatValue b = (FloatValue) stack.pop();
		stack.push(new FloatValue( (double) (b.getDouble().intValue() % a.getDouble().intValue())));
	}

}
