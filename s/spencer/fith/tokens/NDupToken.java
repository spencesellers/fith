package s.spencer.fith.tokens;

import java.util.Iterator;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.FloatValue;
import s.spencer.fith.values.Value;

public class NDupToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		int n = ((FloatValue) stack.pop()).getDouble().intValue();
		LinkedList<Value> items= new LinkedList<Value>();
		for (int i = 0; i < n; i++){
			items.pushFront(stack.pop());
		}
		Iterator<Value> j = items.iterator();
		for (int i = 0; i < n; i++){
			stack.push(j.next());
		}
		j = items.iterator();
		for (int i = 0; i < n; i++){
			stack.push(j.next());
		}
		

	}

}
