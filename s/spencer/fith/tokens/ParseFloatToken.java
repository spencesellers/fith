package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.FloatValue;
import s.spencer.fith.values.StringValue;
import s.spencer.fith.values.Value;

public class ParseFloatToken implements Token {
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		String s = ((StringValue) stack.pop()).getString();
		stack.push(new FloatValue(Double.parseDouble(s)));
	}

}
