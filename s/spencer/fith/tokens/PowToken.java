package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.FloatValue;
import s.spencer.fith.values.Value;

public class PowToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		FloatValue exp = (FloatValue) stack.pop();
		FloatValue base = (FloatValue) stack.pop();
		stack.push(new FloatValue(Math.pow(base.getDouble(), exp.getDouble())));
	}

}
