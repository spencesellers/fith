package s.spencer.fith.tokens;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.StringValue;
import s.spencer.fith.values.Value;

public class PrintToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		Value v = stack.pop();
		String strRep;
		if (v == null){
			System.out.println("Error: Printing null");
			return;
		} else if (v instanceof StringValue){
			strRep = ((StringValue) v).getString();
		} else {
			strRep = v.toString();
		}
		System.out.println(strRep);

	}

}
