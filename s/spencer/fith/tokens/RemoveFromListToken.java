package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.FloatValue;
import s.spencer.fith.values.ListValue;
import s.spencer.fith.values.Value;

public class RemoveFromListToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		int index = ((FloatValue) stack.pop()).getDouble().intValue();
		ListValue list = (ListValue) stack.pop();
		list.getLinkedList().remove(index);
		stack.push(list);

	}

}
