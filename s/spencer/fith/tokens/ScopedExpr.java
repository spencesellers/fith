package s.spencer.fith.tokens;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.Value;

public class ScopedExpr extends Expr{
	public ScopedExpr(){
		super();
	}
	public ScopedExpr(LinkedList<Token> exprs) {
		super(exprs);
	}
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		HeapScope newScope = new HeapScope(scope);
		super.exec(stack, newScope);
	}
	public void execInScope(LinkedList<Value> stack, HeapScope scope){
		super.exec(stack, scope);
	}


}
