package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.FloatValue;
import s.spencer.fith.values.ListValue;
import s.spencer.fith.values.Value;

public class SetIndexToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		int index = ((FloatValue) stack.pop()).getDouble().intValue();
		Value item = stack.pop();
		ListValue list = (ListValue) stack.pop();
		
		
		list.set(item, index);
		stack.push(list);

	}

}