package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.IdentifierValue;
import s.spencer.fith.values.ScopeValue;
import s.spencer.fith.values.Value;

public class SetScopedToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		IdentifierValue idv = (IdentifierValue) stack.pop();
		ScopeValue sv = (ScopeValue) stack.pop();
		Value value = stack.pop();
		
		sv.assign(idv.getIdentifier(), value);
	}

}
