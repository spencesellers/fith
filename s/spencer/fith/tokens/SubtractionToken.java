package s.spencer.fith.tokens;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.FloatValue;
import s.spencer.fith.values.Value;

public class SubtractionToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		Value a = stack.pop();
		Value b = stack.pop();
		if (a instanceof FloatValue && a instanceof FloatValue){
			stack.push(b.subtract(a));
		} else {
			System.out.println("Error: Subtraction not supported on " + b + " and " + a);
		}
	}
	public String toString(){ return "-";}

}
