package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.Value;

public class SwapToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		Value a = stack.pop();
		Value b = stack.pop();
		stack.push(a);
		stack.push(b);

	}

}
