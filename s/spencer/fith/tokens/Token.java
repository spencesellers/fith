package s.spencer.fith.tokens;
import s.spencer.fith.*;
import s.spencer.fith.values.Value;
public interface Token {
	public void exec(LinkedList<Value> stack, HeapScope scope);
}
