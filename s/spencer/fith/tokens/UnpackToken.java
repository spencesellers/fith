package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.IdentifierValue;
import s.spencer.fith.values.Value;

public class UnpackToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		IdentifierValue iv = (IdentifierValue) stack.pop();
		Value value = scope.getValue(iv.getIdentifier());
		stack.push(value);
	}

}
