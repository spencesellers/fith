package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.*;

public class VariableSetToken implements Token {
	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		IdentifierValue idv = (IdentifierValue) stack.pop();
		Value value = stack.pop();
		scope.assignValue(idv.getIdentifier(), value);
		
		assert(value == scope.getValue(idv.getIdentifier())): "Value did not stick!";

	}

}
