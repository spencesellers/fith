package s.spencer.fith.tokens;

import s.spencer.fith.LinkedList;
import s.spencer.fith.HeapScope;
import s.spencer.fith.values.ExecValue;
import s.spencer.fith.values.Value;

public class WhileToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		ExecValue inside = (ExecValue) stack.pop();
		ExecValue conditional = (ExecValue) stack.pop();
		while (true){
			conditional.exec(stack, scope);
			if (stack.peek() == null){
				System.out.println("Error: While: Stack is empty while checking conditional.");
			}
			if (stack.pop().toBoolean()){
				inside.exec(stack, scope);
			} else{
				break;
			}
		}

	}
	public String toString(){ return "while" ;}

}
