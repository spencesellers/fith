package s.spencer.fith.tokens;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.values.ExecValue;
import s.spencer.fith.values.ListValue;
import s.spencer.fith.values.Value;

public class ZipWithToken implements Token {

	@Override
	public void exec(LinkedList<Value> stack, HeapScope scope) {
		ExecValue f = (ExecValue) stack.pop();
		ListValue b = (ListValue) stack.pop();
		ListValue a = (ListValue) stack.pop();
		ListValue result = new ListValue();
		int length = a.getLinkedList().length();
		for (int i = 0; i < length; i++){
			stack.push(a.get(i));
			stack.push(b.get(i));
			f.exec(stack, scope);
			result.add(stack.pop());
		}
		stack.push(result);
	}

}
