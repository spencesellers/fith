package s.spencer.fith.values;

public class BooleanValue implements Value {
	private final boolean value;
	public BooleanValue(boolean b){
		this.value = b;
	}
	public boolean toBoolean() {
		return value;
	}

	@Override
	public Value add(Value x) {
		return null;
	}

	@Override
	public Value subtract(Value x) {
		return null;
	}

	@Override
	public Value multiply(Value x) {
		return null;
	}

	@Override
	public Value divide(Value x) {
		return null;
	}
	public String toString(){
		return Boolean.toString(value);
	}
}
