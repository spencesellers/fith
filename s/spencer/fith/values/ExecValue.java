package s.spencer.fith.values;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.tokens.Expr;
import s.spencer.fith.tokens.ScopedExpr;
import s.spencer.fith.tokens.Token;

public class ExecValue implements Value {
	private Token func;
	public ExecValue(Token token){
		this.func = token;
	}
	public void exec(LinkedList<Value> stack, HeapScope scope){
		func.exec(stack, scope);
	}
	public void execInScope(LinkedList<Value> stack, HeapScope scope){
		if (func instanceof ScopedExpr){
			((ScopedExpr) func).execInScope(stack, scope);
		} else {
			this.exec(stack, scope);
		}
	}
	public void exec(LinkedList<Value> stack){
		func.exec(stack, new HeapScope());
	}
	public boolean toBoolean() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Value add(Value x) {
		if (x instanceof ExecValue && this.func instanceof Expr){
			if (((ExecValue) x).func instanceof Expr){
				return new ExecValue(((Expr)this.func).concat((Expr) ((ExecValue) x).func));
			}
		}
		return null;
	}

	@Override
	public Value subtract(Value x) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value multiply(Value x) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Value divide(Value x) {
		// TODO Auto-generated method stub
		return null;
	}
	public String toString(){
		return "(ExecValue: " + func.toString() + ")";
	}

}
