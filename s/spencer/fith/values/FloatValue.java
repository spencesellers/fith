package s.spencer.fith.values;

public class FloatValue implements Value, Comparable<FloatValue> {
	private final Double value;
	private static final double CLOSE_ENOUGH_TO_ZERO = 0.0001;
	public FloatValue(Double d){
		value = d;
	}
	public FloatValue(FloatValue x){
		value = x.getDouble();
	}
	public Double getDouble(){
		return value;
	}
	public FloatValue add(FloatValue x){
		return new FloatValue(value + x.getDouble());
	}
	public FloatValue multiply(FloatValue x){
		return new FloatValue(value * x.getDouble());
	}
	public FloatValue subtract(FloatValue x){
		return new FloatValue(value - x.getDouble());
	}
	public FloatValue divide(FloatValue x){
		return new FloatValue(value / x.getDouble());
	}
	public boolean toBoolean() {
		return (value >= FloatValue.CLOSE_ENOUGH_TO_ZERO);
	}
	@Override
	public Value add(Value x) {
		if (x instanceof FloatValue){
			return this.add((FloatValue) x);
		} else if (x instanceof ListValue){
			return x.add(this);	
		} else {
			return null;
		}
	}
	@Override
	public Value subtract(Value x) {
		if (x instanceof FloatValue){
			return this.subtract((FloatValue) x);
		} else {
			return null;
		}
	}
	@Override
	public Value multiply(Value x) {
		if (x instanceof FloatValue){
			return new FloatValue(this.getDouble() * ((FloatValue) x).getDouble());
		} return null;
	}
	@Override
	public Value divide(Value x) {
		if (x instanceof FloatValue){
			return new FloatValue(this.getDouble() / ((FloatValue) x).getDouble());
		} return null;
	}
	public String toString(){
		return Double.toString(value);
	}
	@Override
	public int compareTo(FloatValue x) {
		if ( Math.abs(x.getDouble() - this.getDouble()) < FloatValue.CLOSE_ENOUGH_TO_ZERO){
			return 0;
		} else if (x.getDouble() > this.getDouble()){
			return 1;
		} else {
			return -1 ; 
		}
	}

}
