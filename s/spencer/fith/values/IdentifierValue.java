package s.spencer.fith.values;

public class IdentifierValue implements Value {
	private final String id;
	public IdentifierValue(String identifier){
		this.id = identifier;
	}
	public String getIdentifier(){
		return id;
	}
	@Override
	public boolean toBoolean() {
		return false;
	}

	@Override
	public Value add(Value x) {
		return null;
	}

	@Override
	public Value subtract(Value x) {
		return null;
	}

	@Override
	public Value multiply(Value x) {
		return null;
	}

	@Override
	public Value divide(Value x) {
		return null;
	}
	public String toString(){
		return "Identifier $" + this.id;

	}

}
