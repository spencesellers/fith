package s.spencer.fith.values;

import java.util.Iterator;

import s.spencer.fith.LinkedList;


public class ListValue implements Value {
	private final LinkedList<Value> list;
	public ListValue(){
		list = new LinkedList<Value>();
	}
	public void push(Value x){
		list.push(x);
	}
	public Value pop(){
		return list.pop();
	}
	public void set(Value v, int i){
		list.set(v, i);
	}
	public Value get(int index){
		return list.get(index);
	}
	public Value get(FloatValue index){
		return this.get(index.getDouble().intValue());
	}
	public LinkedList<Value> getLinkedList(){
		return list;
	}
	@Override
	public boolean toBoolean() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Value add(Value x) {
		this.push(x);
		return this;
	}

	@Override
	public Value subtract(Value x) {
		return null;
	}

	@Override
	public Value multiply(Value x) {
		return null;
	}

	@Override
	public Value divide(Value x) {
		return null;
	}
	public String toString(){
		Iterator<Value> i = list.iterator();
		String str = "[";
		while (i.hasNext()){
			str = str + i.next().toString() + ", ";
		}
		return str + "]";
	}
}
