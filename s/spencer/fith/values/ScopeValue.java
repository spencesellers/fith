package s.spencer.fith.values;

import s.spencer.fith.HeapScope;
import s.spencer.fith.LinkedList;
import s.spencer.fith.tokens.Token;

public class ScopeValue implements Value{
	public final HeapScope scope;
	public ScopeValue(){
		this.scope = new HeapScope();
	}
	public void assign(String id, Value v){
		scope.assignValue(id, v);
	}
	public Value getToken(String id){
		assert(id != null);
		return scope.getValue(id);
	}
	public void execInScope(Token token, LinkedList<Value> stack){
		token.exec(stack, this.scope);
	}
	public void execInScope(Value value, LinkedList<Value> stack){
		//Doesn't work?
		assert(value != null): "Executing null value";
		if (value instanceof ExecValue){
			((ExecValue) value).exec(stack, this.scope);
		} else {
			stack.push(value);
		}
	}
	public void execIdentifier(String id, LinkedList<Value> stack){
		this.execInScope(scope.getValue(id), stack);
	}
	@Override
	public boolean toBoolean() {
		return true;
	}
	@Override
	public Value add(Value x) {
		return null;
	}
	@Override
	public Value subtract(Value x) {
		return null;
	}
	@Override
	public Value multiply(Value x) {
		return null;
	}
	@Override
	public Value divide(Value x) {
		return null;
	}
	public String toString(){
		String strRep = "(ScopeValue: ";
		String[] ids = scope.getAllIdentifiers();
		for (int i = 0; i < ids.length; i++){
			strRep = strRep + ids[i] + ", ";
		}
		return strRep + " )";
	}


}
