package s.spencer.fith.values;

public class StringValue implements Value {
	private final String value;
	public StringValue (String s){
		this.value = s;
	}
	public String getString(){
		return new String(this.value);
	}
	public StringValue add(StringValue s){
		return new StringValue(this.value + s.getString());
	}
	@Override
	public boolean toBoolean() {
		if (this.value.length() > 0){
			return true;
		}
		return false;
	}
	public StringValue add(Value x) {
		return new StringValue(this.value + x.toString());
	}
	@Override
	public Value subtract(Value x) {
		return null;
	}
	@Override
	public Value multiply(Value x) {
		return null;
	}
	@Override
	public Value divide(Value x) {
		return null;
	}
	public String toString(){
		return "String: \"" + value + "\"";
	}

}
