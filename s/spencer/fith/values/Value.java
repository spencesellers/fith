package s.spencer.fith.values;

public interface Value {
	boolean toBoolean();
	Value add(Value x);
	Value subtract(Value x);
	Value multiply(Value x);
	Value divide(Value x);
	String toString();
}
